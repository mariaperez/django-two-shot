from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


@login_required
def receipt_list(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {
        'receipt_list': receipt_list
    }
    return render(request, 'receipts/home.html', context)


@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser_id = request.user.id

            form.save()
            return redirect('home')
    else:
        form = ReceiptForm()
    context = {
        'form': form
    }
    return render(request, 'receipts/create_receipt.html', context)


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    category_data = []
    for category in categories:
        category_data.append({
            'name': category.name,
        })
    context = {
        'categories': categories
    }
    return render(request, 'receipts/category_list.html', context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    account_data = []
    for account in accounts:
        account_data.append({
            'name': account.name,
            'number': account.number,
        })
    context = {
        'accounts': accounts
    }
    return render(request, 'receipts/account_list.html', context)


@login_required
def create_category(request):
    if request.method == 'POST':
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner_id = request.user.id
            category.save()
            return redirect('category_list')
    else:
        form = ExpenseCategoryForm()
    context = {
        'form': form
    }
    return render(request, 'receipts/create_category.html', context)


@login_required
def create_account(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner_id = request.user.id
            account.save()
            return redirect('account_list')
    else:
        form = AccountForm()
    context = {
        'form': form
    }
    return render(request, 'receipts/create_account.html', context)
